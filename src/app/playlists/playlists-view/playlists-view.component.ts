import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/model/Playlist";
import { PlaylistsService } from "../services/playlists.service";
import { multicast, share } from "rxjs/operators";
import { Subject } from "rxjs";
import { Router } from '@angular/router';

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.css"]
})
export class PlaylistsViewComponent implements OnInit {
  playlists = this.service.getPlaylists();
  selected = this.service.getSelected()

  constructor(
    private router:Router,
    private service: PlaylistsService) {}

  select(playlist: Playlist) {
    // this.service.selectPlaylist(playlist.id);
    
    this.router.navigate(['playlists', playlist.id])
  }

  save(draft: Playlist) {
    this.service.savePlaylist(draft);
  }

  ngOnInit() {
    // Make async pipe resubscribe to new Obsservable
    // setTimeout(()=>{
    //   this.playlists = this.service.getPlaylists();
    // },2000)
  }
}
