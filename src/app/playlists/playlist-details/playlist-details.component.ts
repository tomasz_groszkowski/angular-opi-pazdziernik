import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "src/app/model/Playlist";
import { NgForm } from "@angular/forms";

type Modes = "edit" | "show";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.css"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  constructor() {}

  ngOnInit() {}

  mode: Modes = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistChange = new EventEmitter<Playlist>();

  save(playlistDraft: Partial<Playlist>) {
    const playlist = {
      ...this.playlist,
      ...playlistDraft
    };
    this.mode = "show";
    this.playlistChange.emit(playlist);
  }
}

// C:\Program Files (x86)\Microsoft VS Code\resources\app\extensions\node_modules\typescript\lib\lib.es5.d.ts#1350

/* type partOfPlaylist = Pick<Playlist, "name" | "color" | "favourite">;

type KeysOf<T> = keyof T;

type p = KeysOf<Playlist>;

type Partial<T> = { [k in keyof T]?: T[k] };

type sp = Partial<Playlist>
 */
