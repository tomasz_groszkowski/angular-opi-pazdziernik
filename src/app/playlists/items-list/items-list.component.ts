import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "@/model/playlist";
import { NgForOfContext } from "@angular/common";

interface Item {
  id: number;
  name: string;
  color?: string;
}

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.css"]
})
export class ItemsListComponent<T extends Item> implements OnInit {

  hover: T 

  @Input("items")
  playlists: T[] = [];

  @Input()
  selected: T;

  @Output()
  selectedChange = new EventEmitter<T>();

  select(item: T) {
    this.selectedChange.emit(item);
  }

  myTrackFn(index, item) {
    return item.id;
  }

  constructor() {}

  ngOnInit() {}
}
