import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { PlaylistDetailsComponent } from "./playlist-details/playlist-details.component";
import { ItemsListComponent } from "./items-list/items-list.component";
import { ListItemComponent } from "./list-item/list-item.component";

import {FormsModule} from '@angular/forms';
import { SharedModule } from '../shared/shared.module'
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './containers/playlists/playlists.component';
import { PlaylistComponent } from './containers/playlist/playlist.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    // ng g m shared -m playlists:
    SharedModule,
    PlaylistsRoutingModule
  ],
  declarations: [
    PlaylistsViewComponent,
    PlaylistDetailsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistsComponent,
    PlaylistComponent
  ],
  exports:[
    PlaylistsViewComponent
  ]
})
export class PlaylistsModule {}
