import { Injectable } from "@angular/core";
import { Playlist } from "@/model/playlist";
import { Observable, BehaviorSubject } from "rxjs";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { take } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService implements Resolve<Playlist> {


  resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot){

    const id = parseInt(route.paramMap.get("id"))
    this.selectPlaylist(id);

    return this.getSelected().pipe(
      take(1)
    )
  }


  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favourite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular TOP20",
      favourite: false,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "BEst of Angular",
      favourite: true,
      color: "#00ffff"
    }
  ]);
  private selected = new BehaviorSubject<Playlist>(null);

  // public selected = this.selected.asObservable()

  constructor() {}

  getPlaylists() {
    return this.playlists.asObservable();
  }

  getSelected() {
    return this.selected.asObservable();
  }

  selectPlaylist(id: Playlist["id"]) {
    const playlists = this.playlists.getValue();
    const playlist = playlists.find(p => p.id === id);

    if (playlist) {
      this.selected.next(playlist);
    }
  }

  savePlaylist(draft: Playlist) {
    const playlists = this.playlists.getValue();

    const index = playlists.findIndex(p => p.id === draft.id);
    if (index !== -1) {
      playlists.splice(index, 1, draft);
    }
    this.playlists.next(playlists);

    this.selectPlaylist(draft.id);
  }
}
