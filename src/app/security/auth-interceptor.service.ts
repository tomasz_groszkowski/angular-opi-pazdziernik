import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError, never, NEVER } from "rxjs";
import { catchError } from "rxjs/operators";
import { SecurityService } from "./security.service";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private security: SecurityService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    
    const authReq = req.clone({
      setHeaders: {
        Authorization: "Bearer " + this.security.getToken()
      }
    });

    return next.handle(authReq).pipe(
      catchError((error, caughtSource) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          this.security.authorize();
        }

        return throwError(error.error.error);
      })
    );
  }
}
