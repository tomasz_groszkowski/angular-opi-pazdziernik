import { Directive } from '@angular/core';
import { MusicSearchService } from './services/music-search.service';

@Directive({
  selector: '[appSearchProvider]',
  providers:[
    MusicSearchService
  ]
})
export class SearchProviderDirective {

  constructor() { }

}
