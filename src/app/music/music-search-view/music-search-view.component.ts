import { Component, OnInit, Inject } from "@angular/core";
import { MusicSearchService } from "../services/music-search.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.css"]
  // providers:[
  //   MusicSearchService
  // ]
})
export class MusicSearchViewComponent implements OnInit {
  // albums: Album[];
  albumsChanges = this.searchService.getAlbums();
  // .pipe(tap(albums => (this.albums = albums)));

  queryChanges = this.searchService.getQuery();

  message: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchService: MusicSearchService
  ) {}

  ngOnInit() {
    const query = this.route.snapshot.queryParamMap.get("query");
    if(query){
      this.searchService.search(query);
    }
  }

  search(query: string) {
    this.searchService.search(query);

    this.router.navigate([], {
      queryParams: {
        query: query
      },
      // queryParamsHandling:'merge',
      relativeTo: this.route
    });
  }
}
// shareReplay()
// multicast(()=>new /* Replay */Subject()),
// refCount()
