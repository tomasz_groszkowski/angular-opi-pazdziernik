import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MusicSearchViewComponent } from "./music-search-view/music-search-view.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "src/environments/environment";
import { SEARCH_API_URL, MusicSearchService } from './services/music-search.service';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { SearchProviderDirective } from "./search-provider.directive";
import { MusicRoutingModule } from "./music-routing.module";
import { SharedModule } from "../shared/shared.module";
import { SecurityModule } from '../security/security.module';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    MusicRoutingModule,
    SharedModule,
    SecurityModule.forChild()
  ],
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent,
    SearchProviderDirective
  ],
  exports: [MusicSearchViewComponent, SearchProviderDirective],
  providers: [
    {
      provide: SEARCH_API_URL,
      useValue: environment.search_api_url
    },
    MusicSearchService
    // {
    //   provide:HttpClient,
    //   useClass: MyHttpClient
    // },
    // {
    //   provide: "MusicSearchService",
    //   useFactory: (url: string) => {
    //     return new MusicSearchService(url);
    //   },
    //   deps: ["SEARCH_API_URL"]
    // }
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService
    //   // deps: ["SEARCH_API_URL"]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService
    // },
  ]
})
export class MusicModule {}
