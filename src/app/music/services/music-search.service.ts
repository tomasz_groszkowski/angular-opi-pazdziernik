import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album } from "@/model/album";
import { HttpClient } from "@angular/common/http";
import { AlbumsResponse } from "../../model/album";
import {
  map,
  concat,
  startWith,
  switchMap,
  filter,
  catchError,
  debounceTime,
  distinctUntilChanged
} from "rxjs/operators";
import { of, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

export const SEARCH_API_URL = new InjectionToken("Search api url");

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  albumsChange = new BehaviorSubject<Album[]>([]);
  queryChange = new BehaviorSubject<string>("batman");

  constructor(
    @Inject(SEARCH_API_URL) private api_url: string,
    private http: HttpClient
  ) {
    this.queryChange
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter(query => query.length >= 3),
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http
            .get<AlbumsResponse>(this.api_url, {
              params
            })
            .pipe(
              catchError(() => {
                return [];
              })
            )
        ),
        map(resp => resp.albums.items)
      )
      // .subscribe(this.albumsChange)
      .subscribe(albums => {
        this.albumsChange.next(albums);
      });
  }

  // Input:
  search(query: string) {
    this.queryChange.next(query);
  }

  // Output:
  getAlbums() {
    return this.albumsChange.asObservable();
  }

  getQuery() {
    return this.queryChange.asObservable();
  }
}
