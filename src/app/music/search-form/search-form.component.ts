import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import {
  AbstractControl,
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  distinctUntilChanged,
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";
import { debug } from "util";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  constructor(private bob: FormBuilder) {
    // censor:: badword => control => error
    const censor = (badword: string): ValidatorFn => {
      return (control: AbstractControl): ValidationErrors | null => {
        const hasError = (control.value as string).includes(badword);

        return hasError
          ? {
              censor: { badword /* : badword */ }
            }
          : null;
      };
    };

    const asyncCensor = (badword: string): AsyncValidatorFn => {
      // return this.http.get('/validate',control.value).pipe(map(res=>valiErr))
      // return of((control.value as string).includes(badword)).pipe(delay(2000),take(1))

      return (control): Observable<ValidationErrors> => {
        return Observable.create(
          (observer: Observer<ValidationErrors | null>) => {
            // onSubscribe:
            const hasError = (control.value as string).includes(badword);
            const handler = setTimeout(() => {
              observer.next(hasError ? { censor: { badword } } : null);
              observer.complete();
            }, 2000);

            // onUnsubscribe:
            return () => {
              clearTimeout(handler);
            };
          }
        );
      };
    };

    this.queryForm = this.bob.group({
      query: new FormControl(
        "",
        [
          Validators.required,
          Validators.minLength(3)
          // censor("batman")
        ],
        [asyncCensor("batman")]
      )
    });

    // console.log(this.queryForm);

    const queryField = this.queryForm.get("query");

    const valueChanges = queryField.valueChanges.pipe(
      // filter(query => query.length >= 3),
    );

    const validChanges = queryField.statusChanges.pipe(
      filter(status => status === "VALID")
    );

    const searchChanges = validChanges.pipe(
      // combineLatest(valueChanges, (valid, value) => value)
      withLatestFrom(valueChanges, (valid, value) => value)
    );

    searchChanges.subscribe(query => this.search(query));
  }

  @Input()
  set query(query) {
    this.queryForm.get("query").setValue(query, {
      onlySelf: false,
      // prevent valueChange / sending request:
      emitEvent: false
    });
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}

  // formFieldsDef = [{}]
  /*   fieldDefs = this.getBuilder()
          .text('query','Label')
          .checkbox()
          .select()
        .build() */
  // form = this.buildModel(this.fieldDefs);
  // <auto-form [fields]="formFieldsDef"></auto-form>
}
