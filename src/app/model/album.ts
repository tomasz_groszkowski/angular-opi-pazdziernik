interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: ImageObject[];
  artists?: Artist[];
}

export interface Artist extends Entity {
  images: ImageObject[];
}

export interface ImageObject {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
  total: number;
  limit: number;
  offset: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}

// https://developer.spotify.com/documentation/web-api/reference/search/search/
// http://www.jsontots.com/
// https://www.npmjs.com/package/swagger-ts-generator

/* 
export class Album implements Album {
  constructor(
    public id: string, 
    public name = ''
  ) {}
}
 */

/*  class AlbumMapper{
   entityToDto(){}
   dtoToEntity(){}
 } */
