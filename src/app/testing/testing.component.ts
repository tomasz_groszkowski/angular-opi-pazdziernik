import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css'],
})
export class TestingComponent implements OnInit {

  message = 'testing works!'

  constructor(
    @Inject('MyService') private service
  ) { 
  }
  
  getMessage(){
    this.message = this.service.getMessage()

  }


  ngOnInit() {
  }

}
