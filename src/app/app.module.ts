import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { SecurityModule } from './security/security.module';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestingComponent } from './testing/testing.component';


@NgModule({
  declarations: [
    AppComponent,
    TestingComponent,
  ],
  imports: [
    // HttpClientModule,
    BrowserModule,
    PlaylistsModule,
    SharedModule,
    // MusicModule,
    AppRoutingModule,
    SecurityModule.forRoot(environment.security)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  // constructor(private http:HttpClient){  }

  // constructor(private app: ApplicationRef) {}
  // ngDoBootstrap() {
  //   this.app.bootstrap(AppComponent);
  // }
}
