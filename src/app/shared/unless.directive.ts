import { Directive, TemplateRef, ViewContainerRef, Input } from "@angular/core";

type Context = {
  $implicit: string;
  message: string;
};

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  @Input()
  set appUnless(hide: boolean) {
    if (hide) {
      this.vcr.clear()
    } else {
      this.vcr.createEmbeddedView(this.tpl, {
        $implicit: "Placki",
        message: "ciastka"
      },0);
    }
  }

  constructor(
    private tpl: TemplateRef<Context>,
    private vcr: ViewContainerRef
  ) {}

}
