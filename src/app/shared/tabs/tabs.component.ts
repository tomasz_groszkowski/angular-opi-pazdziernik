import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ContentChildren,
  QueryList
} from "@angular/core";
import { TabsNavComponent } from "../tabs-nav/tabs-nav.component";
import { TabComponent } from "../tab/tab.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.css"]
})
export class TabsComponent implements OnInit {
  @ViewChild(TabsNavComponent)
  navRef: TabsNavComponent;

  // w <ng-content></ng-content>
  // @ContentChild(TabsNavComponent)
  // navRef:TabsNavComponent

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  active = null;

  activate(tab) {
    this.active = tab;
  }

  constructor() {}

  updateTabs(tab) {
    this.tabs.forEach(tab => {
      tab.active = tab == this.active;
    });
  }

  ngOnInit() {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.navRef.links = this.tabs.map(tab => {
        return {
          title: tab.title
        };
      });
    });
    this.tabs.forEach(tab => {
      tab.activeChange.subscribe(() => {
        this.active = tab;
        this.updateTabs(tab);
      });
    });
  }
}
