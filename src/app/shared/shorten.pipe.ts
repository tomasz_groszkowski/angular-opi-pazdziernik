import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  pure: false
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, length = 20): any {

    // return Math.random()
    return value.substr(0,length);
  }

}
