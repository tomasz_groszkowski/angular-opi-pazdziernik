import { AuthConfig } from '../app/security/security.service';
export const environment = {
  production: true,
  search_api_url: "https://api.spotify.com/v1/search",
  /* === */
  security: {
    auth_url: "https://accounts.spotify.com/authorize",
    params: {
      client_id: "1cece95f36524d7ab4effad0b7424839",
      redirect_uri: "http://localhost:4200/",
      response_type: "token"
    }
  } as AuthConfig
};
